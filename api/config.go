package api

import (
	"fmt"
	"os"

	"github.com/dghubble/go-twitter/twitter"
)

func ConfigRequest(query string) *twitter.SearchTweetParams {
	params := twitter.SearchTweetParams{
		Query:      query,           //512 characters
		Count:      100,             //max 100
		Lang:       "pt",            //idiom
		TweetMode:  "extended",      //text complete
		Until:      getUntil(),      //yyyy-MM-dd
		ResultType: getResultType(), //default
	}
	fmt.Printf("SearchTweetParams: count=%d, lang=%s, query=%s\n", params.Count, params.Lang, params.Query)
	return &params
}

func getUntil() string {
	untilEnv := os.Getenv("UNTIL_DATE")
	return untilEnv
}

func getResultType() string {
	if os.Getenv("UNTIL_DATE") == "" {
		return "recent"
	} else {
		return "mixed"
	}
}
