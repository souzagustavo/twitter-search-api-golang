package api

import (
	"os"
	"strings"
)

type Game struct {
	Name  string
	Query string
}

func (g Game) FileName() string {
	return strings.Join([]string{os.Getenv("OUTPUT_FOLDER"), g.Name, ".csv"}, "")
}

func (g Game) FileIdName() string {
	return strings.Join([]string{os.Getenv("OUTPUT_FOLDER"), g.Name, "-id.csv"}, "")
}

func (g Game) FileBackupName() string {
	return strings.Join([]string{os.Getenv("OUTPUT_FOLDER"), g.Name, "-backup.csv"}, "")
}
