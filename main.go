package main

// OAuth1
import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"twitter-search-api-golang/api"
	"twitter-search-api-golang/utils"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/joho/godotenv"
	"github.com/robfig/cron/v3"
)

func getGames() []api.Game {
	return []api.Game{
		{Name: "call-of-duty", Query: "Call of Duty Warzone OR Warzone OR warzone OR #warzone OR #cod OR #callofduty OR #modernwarfare OR #BlackOpsColdWar OR Verdansk"},
		{Name: "fortnite", Query: "fortnite OR Fortnite OR #fortnite OR #fortnitecommunity OR #fortnitebr OR #fortnitenews"},
		{Name: "gta-v", Query: "gtav OR gta OR #GTAOnline OR #gta OR #GTAV"},
		{Name: "valorant", Query: "valorant OR #valorant OR Valorant"},
		{Name: "counter-strike-go", Query: "Counter strike OR CSGO OR #CSGO"},
		{Name: "minecraft", Query: "minecraft OR Minecraft OR #minecraft"},
		{Name: "league-of-legends", Query: "league of legends OR League of Legends"},
		{Name: "free-fire", Query: "free fire OR #freefire"},
		{Name: "rocket-league", Query: "rocket league OR Rocket League OR #rocketleague"},
		{Name: "god-of-war", Query: "god of war OR God of War OR #gow OR #godofwar"},
		{Name: "escape-from-tarkov", Query: "Escape from Tarkov"},
		{Name: "super-people", Query: "super people OR Super People OR #superpeople"},
	}
}

func main() {
	godotenv.Load()
	cron := cron.New()
	cron.AddFunc(os.Getenv("CRON_SEARCH"), twitterSearch)
	cron.AddFunc(os.Getenv("CRON_BACKUP"), backup)
	cron.Start()

	select {}
}

func twitterSearch() {
	games := getGames()
	client := api.Auth()

	for _, game := range games {
		gameSearch(client, game)
	}
}

func gameSearch(client *twitter.Client, game api.Game) {
	params := api.ConfigRequest(game.Query)

	search, resp, err := client.Search.Tweets(params)
	if err != nil {
		log.Println(err)
		return
	}

	f := utils.OpenFile(game.FileName())
	defer f.Close()

	newIDs := []string{}

	log.Printf("Response: tweets: %d, status_code: %d\n", len(search.Statuses), resp.StatusCode)
	for _, tweet := range search.Statuses {
		if containsTweet(game.FileIdName(), tweet.IDStr) || tweet.RetweetedStatus != nil {
			continue
		}
		line := utils.FormattedLine(tweet)

		if _, err := f.WriteString(line); err != nil {
			log.Println(err)
		} else {
			newIDs = append(newIDs, tweet.IDStr)
		}
	}
	saveIDs(newIDs, game.FileIdName())
}

func saveIDs(ids []string, fileIdName string) {
	log.Printf("New tweets: %d\n", len(ids))

	f := utils.OpenFile(fileIdName)
	defer f.Close()

	for _, id := range ids {
		if _, err := f.WriteString(fmt.Sprintf("%s\n", id)); err != nil {
			log.Fatal(err)
		}
	}
}

func containsTweet(backup, tweetID string) bool {
	f, err := os.Open(backup)
	if err != nil {
		return false
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		id := strings.TrimSuffix(scanner.Text(), "\n")
		if id == tweetID {
			return true
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return false
}

func backup() {
	games := getGames()

	for _, game := range games {
		input, err := ioutil.ReadFile(game.FileName())
		if err != nil {
			log.Println(err)
			return
		}

		err = ioutil.WriteFile(game.FileBackupName(), input, 0644)
		if err != nil {
			fmt.Println(err)
			return
		}
		log.Printf("Game %s: backup completed\n", game.Name)
	}
}
