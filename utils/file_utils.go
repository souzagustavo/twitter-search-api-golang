package utils

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/dghubble/go-twitter/twitter"
)

func OpenFile(fileName string) *os.File {
	f, err := os.OpenFile(fileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	return f
}

func FormattedLine(tweet twitter.Tweet) string {
	text := strings.ReplaceAll(tweet.FullText, "\n", " ")
	text = strings.ReplaceAll(text, ";", " ")

	return fmt.Sprintf("%s;%s;%s;%s\n", tweet.IDStr, tweet.CreatedAt, tweet.User.ScreenName, text)
}
