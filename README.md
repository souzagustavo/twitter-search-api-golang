## Twitter Search API - Golang

Add file `.env` with Environment variables to root folder:
```
#Consumer
CONSUMER_KEY=<VALUE>
CONSUMER_SECRET=<VALUE>

#Authorization
ACCESS_TOKEN=<VALUE>
ACCESS_SECRET=<VALUE>

#Result
OUTPUT_FOLDER=<PATH>

#Cron
CRON_SEARCH=@every 30m
CRON_BACKUP=@every 1h

#Request
UNTIL_DATE=<YYYY-MM-DD>
```

How to run:
```
go run main.go
```