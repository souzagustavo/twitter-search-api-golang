module twitter-search-api-golang

go 1.14

require (
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/dghubble/go-twitter v0.0.0-20201011215211-4b180d0cc78d
	github.com/dghubble/oauth1 v0.7.0
	github.com/joho/godotenv v1.3.0
	github.com/robfig/cron/v3 v3.0.0
)
